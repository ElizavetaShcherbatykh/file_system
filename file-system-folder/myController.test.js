const fs = require('fs');
const util = require('util');

const {
    Controller
} = require('./file-system-folder/myController');

let myController = new Controller('family.json');

const firstMember = { name: 'Mari', surname: 'Seccc', group: "pets" };
const newMember = { name: 'Mar', surname: 'Seccc', group: "pets", favourites: [{ name: 'Mari', surname: 'Seccc' }], owner: { name: 'Mari', surname: 'Seccc' } };

const readFile = util.promisify(fs.readFile);

test('File was created with the valid structure, family member has correct params and was added', async () => {
    await myController.addNewFamilyMember(firstMember);
    const fileData = await readFile('family.json', 'utf8').catch(e => console.log(e));
    const { family } = JSON.parse(await readFile('family.json'));
    const wrongParams = family.find(item => {
        return ((item.favourites !== undefined) || (item.owner !== undefined));
    });
    const match = family.find(item => {
        return ((item.name === firstMember.name) && (item.surname === firstMember.surname))
    });
    expect(typeof fileData === 'string').toBeTruthy();
    expect(wrongParams).toBeUndefined();
    expect(match).not.toBeUndefined();
});

test('New family member was added with correct params', async () => {
    await myController.addNewFamilyMember(newMember);
    const { family } = JSON.parse(await readFile('family.json', 'utf8'));
    const addedFamilyMember = family.find(item => {
        return ((item.name === newMember.name) && (item.surname === newMember.surname));
    });
    const match = [];
    addedFamilyMember.favourites.forEach(fav => {
        let matchFav = family.find(member => {
            return (member.id === fav);
        })
        if (matchFav !== undefined) {
            match.push(matchFav)
        }
    })
    const matchOwner = family.find(el => {
        return ((el.id === addedFamilyMember.owner))
    });
    expect(match.length === addedFamilyMember.favourites.length).toBeTruthy();
    expect(addedFamilyMember).not.toBeUndefined();
    expect(matchOwner).not.toBeUndefined();
});

