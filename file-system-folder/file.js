const fs = require('fs');
const util = require('util');

const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);
const mkdir = util.promisify(fs.mkdir);
const unlink = util.promisify(fs.unlink);
const rename = util.promisify(fs.rename);


// File creation

async function createTxtFile() {
    await writeFile('example.txt', "Hello World!");
    return console.log('File created')
}

// File reading

async function readTxtFile() {
    const data = await readFile('example.txt', 'utf8');
    return console.log(data)
}

// File renaming

async function renameFile() {
    await rename('example.txt', 'new-example.txt');
    return console.log('File renamed');
}

// File deleting

async function deleteFile() {
    await unlink('new-example.txt');
    return console.log('File was deleted');
}

// Creation of index.html in directions with a standart markup entry

async function newHtml() {
    const buildHtml = () => {
        const html =
        `<!DOCTYPE html>\n<html>\n\t<head>\n\t\t<title>Title</title>\n\t</head>\n\t<body>\n<ul>\t\t</ul>\n\t</body>\n</html>`
        return html
    };

    try {
        await mkdir('./src/html', {recursive: true});
        const html = buildHtml();
        await writeFile('./src/html/index.html', html);
        return console.log('HTML-file created')
    } catch (err) {
        console.log(err)
    }
}

// Adding in index.html a list of products with styles

async function addList() {

    try {
        let data = await readFile('./src/html/index.html', 'utf8')
        const list = ["Milk", "Bread", "Meat"];
        const dataToAdd = list.map(item => `\t\t\t<li>${item}</li>\n`).join('');

        const styleToAdd =
        `<style type="text/css">
            li {font-weight: 600; font-size: 20px;}
        </style>`
        data = data.replace(/<ul>/g, '\t\t<ul>' + `\n${dataToAdd}`);
        data = data.replace(/<\/title>/g, '</title>' + `\n\t\t${styleToAdd}`);
        await writeFile('./src/html/index.html', data)
        return console.log('List added')
    } catch (err) {
        console.log(err)
    }
}

// File mooving

async function moveFile() {

    try {
        await mkdir('./build/html', {recursive: true});
        await rename('./src/html/index.html', './build/html/index.html')
        return console.log('File moved')
    } catch (err) {
        console.log(err)
    }
}

// Methods call

async function callAllMethods() {
    await createTxtFile();
    await readTxtFile();
    await renameFile();
    await deleteFile();
    await newHtml();
    await addList();
    await moveFile();
    return console.log('Done!')
}

callAllMethods();