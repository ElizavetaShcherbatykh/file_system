const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const access = util.promisify(fs.access);

const structureKeys = ["family", "group"];

const structure = {
    family: [],
    group: [
        { id: 1, name: "parents" },
        { id: 2, name: "children" },
        { id: 3, name: "pets" }
    ]
}

class Controller {
    constructor(fileName) {
        this.fileName = fileName;
        this.familyFile = null;
    }

    checkNewFamilyMemberParams(newFamilyMember, familyArray) {
        try {
            const doubleFamilyMember = familyArray.find((double) => {
                return ((double.name === newFamilyMember.name) && (double.surname === newFamilyMember.surname))
            });
            if (doubleFamilyMember !== undefined) {
                throw new Error('Family member with these name and surname already exist')
            } else {
                if (newFamilyMember.favourites === undefined) {
                    return
                } else {
                    const favId = [];
                    newFamilyMember.favourites.forEach((favourite) => {
                        let answer = familyArray.find((fav) => {
                            return ((fav.name === favourite.name) && (fav.surname === favourite.surname))
                        });

                        if (answer !== undefined) {
                            favId.push(answer.id)
                        } else {
                            throw new Error(`Favourite ${favourite.name} does not exist`);
                        }
                    })
                    newFamilyMember.favourites = favId;
                }
                if (newFamilyMember.owner === undefined) {
                    return
                } else {
                    const ownerObject = familyArray.find(member => {
                        return ((member.name === newFamilyMember.owner.name) && (member.surname === newFamilyMember.owner.surname))
                    });
                    if (ownerObject !== undefined) {
                        newFamilyMember.owner = ownerObject.id
                    } else {
                        throw new Error('Owner does not exist');
                    }
                }
            }
        } catch (err) {
            console.log(err.message)
            throw "Try again";
        }
    }

    checkNewFamilyMemberGroup(newFamilyMember, familyArray, familyGroups) {
        try {
            const result = familyGroups.find(grp => {
                return (grp.name === newFamilyMember.group);
            });
            if (result !== undefined) {
                newFamilyMember.group = result.id;
                return familyArray.push(newFamilyMember);
            } else {
                throw new Error('Group does not exist');
            }
        } catch (err) {
            console.log(err.message)
            throw "Try again";
        }
    }

    async init() {
        try {
            await access(this.fileName);
            const checkedData = JSON.parse(await readFile(this.fileName, 'utf8'));
            this.familyFile = checkedData;
            const checkedStructure = Object.keys(this.familyFile);
            if (checkedStructure.toString() !== structureKeys.toString()) {
                throw new Error('File with this name already exist and it\'s structure isn\'t valid');
            } else {
                return
            }
        } catch (err) {
            if (err.code === "ENOENT") {
                this.familyFile = structure;
            } else {
                console.log('File with this name already exist and it\'s structure isn\'t valid');
                throw "Try again";
            }
        }
    }

    async addNewFamilyMember(newFamilyMember) {
        try {
            await this.init();
            const familyArray = this.familyFile.family;
            const familyGroups = this.familyFile.group;

            if (familyArray.length === 0) {
                if (newFamilyMember.favourites !== undefined || newFamilyMember.owner !== undefined) {
                    throw new Error('The first family member can\'t has favourites or owner');
                } else {
                    this.checkNewFamilyMemberGroup(newFamilyMember, familyArray, familyGroups);
                    newFamilyMember["id"] = familyArray.length;
                }
            } else {
                this.checkNewFamilyMemberParams(newFamilyMember, familyArray);
                this.checkNewFamilyMemberGroup(newFamilyMember, familyArray, familyGroups);
                newFamilyMember["id"] = familyArray.length;
            }
            const dataToAdd = JSON.stringify(this.familyFile);
            writeFile(this.fileName, dataToAdd);
            return console.log('Family member created');
        } catch (err) {
            console.log(err.message);
        }
    }

    async readFamily() {
        const readFile = util.promisify(fs.readFile);
        try {
            const data = await readFile(this.fileName, 'utf8');
            return data;
        } catch (err) {
            console.log('File does not exist')
        }
    }
}

const myController = new Controller('family.json');
// myController.addNewFamilyMember({ name: 'Mari', surname: 'Secc', group: "pets", favourites: [{ name: 'Mar', surname: 'Sec' }], owner: { name: 'Mar', surname: 'Sec' } })
// myController.readFamily()

module.exports = {
    Controller,
};