const http = require('http'),
    fs = require('fs'),
    util = require('util'),
    path = require('path');

const stat = util.promisify(fs.stat);
const readFile = util.promisify(fs.readFile);
const readdir = util.promisify(fs.readdir);

const createHtml = (fileName) => `<!DOCTYPE html>
<head><title>File System</title></head>
<body>${fileName}</body></html>`

async function requestListener(req, res) {
    try {
        let filePath = req.url;
        if (req.url == '/') {
            filePath = './';
        } else {
            filePath = path.join('./', req.url);
        }

        const correctPath = req.url !== '/' ? filePath + '/' : filePath

        const stats = await stat(filePath);
        const isDirectory = stats.isDirectory();
        if (isDirectory) {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            const files = await readdir(filePath);
            const filesArr = files.map(item => `<a href="/${correctPath + item}">${item}</a><br/>`);
            res.write(createHtml(filesArr.join('')));
        } else {
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            const fileData = await readFile(filePath, 'utf-8');
            res.write(fileData);
        };
        res.end();
    } catch (err) {
        res.writeHead(404);
        res.end(err.message);
    }
}

const server = http.createServer(requestListener);
server.listen(3001, () => console.log('Server is on'));
server.on('error', function (e) {
    console.log(e.message);
});
